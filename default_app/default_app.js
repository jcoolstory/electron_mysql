var app = require('app');

var BrowserWindow = require('browser-window');

var mainWindow = null;
var http = require('http');

http.createServer(function (request,response){
  response.writeHead(200,{'Content-Type':'text/html'});
  response.end('<h1>hello world... !<h2>');
}).listen(52273,function(){
  console.log('server running as http://127.0.0.1:52273 ');
})


// Quit when all windows are closed.
app.on('window-all-closed', function() {
  app.quit();
});

app.on('ready', function() {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    'auto-hide-menu-bar': true,
    'use-content-size': true,
  });
  mainWindow.loadUrl('file://' + __dirname + '/index.html');
  mainWindow.focus();
});
