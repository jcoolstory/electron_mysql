var mysql = require('mysql');
var client;

initdb =  function(info,callback){
  client = mysql.createConnection(info)
	
  console.log(info)
  return client
}

disconnectdb = function(){
  client.destroy()
}

  excutequery = function(str, callback){
    client.query(str,function(error,result){
      if (error == undefined)
      {
        callback(result);
      }else{
        console.log("error :" + error)
      }
    });
  }
  var rootscope = ''
  var remote = require("remote")
  var ipc = require('ipc');
  var tableshowsql = "show tables;"

  function getFiles (dir, files_){
    return fs.readdirSync(dir);;
  }

  var app = angular.module('sampleApp',['ngDialog']);
  function childCtrl($scope, ngDialog){
	$scope.dbinfo = ({
	  host:'hbrgo.com',
      port:'12087',
      user:'temp',
      password:'1234',
      database:'temp'
    })
    $scope.sql = 'show tables;'
    $scope.changeParentName = function(){
      $scope.name =getFiles('.');
    };
    $scope.open = function () {
      ngDialog.open({
	    template: 'firstDialog',
	    controller: 'childCtrl',
	    className: 'ngdialog-theme-default'
	  });
    };
	
	$scope.updatedbinfo = function(dbinfo) {
		
        $scope.dbinfo =dbinfo;

		ngDialog.close('ngdialog1');
      };

      $scope.resetdbinfo = function() {
        $scope.dbinfo = angular.copy($scope.dbinfo);
		$scope.$apply();
    };

    $scope.connectdb = function(){
      $scope.connect = initdb($scope.dbinfo);
      excutequery(tableshowsql,function(result){
        $scope.tables = [];
        $scope.tables.columns =Object.keys(result[0]);
        $scope.tables.rows = result;
        $scope.$apply()
      })
    }
	
    $scope.getdb = function(){
      excutequery($scope.sql,function(result){
		if (result.length ==0){
          $scope.columns =['message'];
          $scope.rows = [{"message":'empy row'}];
		}
		else{		  
          $scope.columns =Object.keys(result[0]);
          $scope.rows = result;
		}
	    $scope.$apply()
      })
    }

	$scope.changeselecquery = function(col){
		$scope.sql= "select * from " +col +";";
	}

    rootscope = $scope;
    $scope.line=[]
    ipc.on('asynchronous-reply', function(arg) {
      console.log('asynchronous-reply : ' + arg); // prints "pong"
      $scope.line.push(arg)
      $scope.$apply()
    });
  };
